Data copied straight from our [pitstop](https://helloworldopen.com/pitstop).

Handy because the timeout is annoying, so might as well keep some of
the data copied here, except for secure stuff like botkeys, etc...



## TEST SERVER (LOAD BALANCER): 

 * testserver.helloworldopen.com

## MORE TEST SERVERS: 

 * balances nodes (I think):
  * hakkinen.helloworldopen.com (R2)
  * senna.helloworldopen.com (R1)
  * webber.helloworldopen.com (R3)
 * prost.helloworldopen.com (constant physics for testing!)

Note that unless otherwise noted, the test servers might have varying
physics laws during their test runs. Not just different car/tracks:
actually different coefficients on several variables (e.g. friction
tolerance).

## BITBUCKET REPOSITORY:

 * hwo2014/hwo2014-team-928

## TEST TRACKS AVAILABLE: 

 * Finland (keimola)
 * Germany (germany)
 * U.S.A. (usa)


# Team Links

 * [Public Team Page](https://helloworldopen.com/team/928)
 * [Private Team Page / PitStop](https://helloworldopen.com/pitstop)
 * [CI Runs](https://helloworldopen.com/pitstop/ci-runs)
 * [Test Races](https://helloworldopen.com/pitstop/testraces)
 * [BitBucket Repo](https://bitbucket.org/hwo2014/hwo2014-team-928/)

# HWO Official Links

 * [HWO](http://www.helloworldopen.com/)
 * [HWO Blog](http://blog.helloworldopen.com/)
 * [HWO Reddit](http://www.reddit.com/r/HWO/)
 * [Teams](https://helloworldopen.com/teams)
 * [Rules](https://helloworldopen.com/rules)
 * [TechSpec](https://helloworldopen.com/techspec)

# Some Tricks

 * games can be [downloaded][1] using their {{gameId}} ([example][2]).
 * games can be visualized in full-screen using the [visualizer][3]
   ([example][4]).

[1]: https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/<gameId>.json 
[2]: https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/4b2d4ef4-6d74-442c-8ebe-14d3e45d1c28.json
[3]: https://helloworldopen.com/race-visualizer/?recording=https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/<gameId>.json
[4]: https://helloworldopen.com/race-visualizer/?v=1398419304547&recording=https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/4b2d4ef4-6d74-442c-8ebe-14d3e45d1c28.json
