#!/bin/sh

. ../config


CI_DATE=$(date +"%Y%m%d%H%M")
CI_RACES=(
  keimola
  germany
  usa
  france
  elaeintarha
  imola
  england
  suzuka
  pentag
)

function rDefault() {
  if [ -z "$2" ]; then
    echo "$1";
  else
    echo "$2";
  fi
}

function race() {
  botname=$(rDefault "test" "$1")
  botkey=$(rDefault "${BOTKEY}" "$2")
  race=$(rDefault "keimola" "$3")
  cars=$(rDefault "1" "$4")
  mode=$(rDefault "" "$5")
  date=$(date +"%Y%m%d%H%M")

  echo " > [${botname}] racing on ${race} with ${cars} cars (${date})"
  ./run                                  \
    testserver.helloworldopen.com 8091   \
      ${botname} ${botkey}               \
      --track=${race}                    \
      --carCount=${cars}                 \
      --isVerbose=true                   \
      --logLevel=debug                   \
      "${mode}"                          \
      &> tests-${CI_DATE}-${race}-${cars}-${botname}-${date}.log

  cat tests-${CI_DATE}-${race}-${cars}-${botname}-${date}.log >> tests-${CI_DATE}.log
}

function createRace() {
    race "${1}" "${2}" "${3}" "${4}" "--createRace";
}

function joinRace() {
    race "${1}" "${2}" "${3}" "${4}";
}


function preamble() {
    echo "====================================="
    echo "Continuous Integration Run ${CI_DATE}"
    echo ""
    echo " Bot: ${1}"
    echo " Key: ${2}"
    echo ""
    echo " CI Mode  : ${3}"
    echo " Bot Mode : ${4}"
    echo " Car Count: ${5}"
}

function main() {
    ci_mode=$(rDefault "all" "$1"); # (default-race, solo-races, match-races, all)
    cars=$(rDefault "4" "$2");
    bot_mode=$(rDefault "join" "$3");

    preamble "${BOTNAME}" "${BOTKEY}" "${ci_mode}" "${bot_mode}" "${cars}";
    if [ "${ci_mode}" = "default-race" ] || [ "${ci_mode}" = "all" ]; then
        echo "-------------------------------------"
        echo " default-race:"
        race "${BOTNAME}" "${BOTKEY}" "";
        race "${BOTNAME}" "${BOTKEY}" "" "$cars" "$bot_mode";
    fi

    if [ "${ci_mode}" = "solo-races" ] || [ "${ci_mode}" = "all" ]; then
        echo "-------------------------------------"
        echo " solo races:"
        for RACE_NAME in ${CI_RACES[@]}; do
            race "${BOTNAME}" "${BOTKEY}" "${RACE_NAME}";
        done
    fi

    if [ "${ci_mode}" = "match-races" ] || [ "${ci_mode}" = "all" ]; then
        echo "-------------------------------------"
        echo " match races:"
        for RACE_NAME in ${CI_RACES[@]}; do
            if [ "${bot_mode}" == "create-and-join" ]; then
                {
                    createRace "${BOTNAME}-1" "${BOTKEY}" "${RACE_NAME}" "$cars";
                } &
                current_car=2;
                while [ ${current_car} -lt 5 ]; do
                    {
                        joinRace "${BOTNAME}-${current_car}" "${BOTKEY}" "${RACE_NAME}" "$cars";
                    } &
                    current_car=$((${current_car}+1))
                done
                wait;
            elif [ "$BOT_MODE" == "create" ]; then
                createRace "${BOTNAME}" "${BOTKEY}" "${RACE_NAME}" "$CARS";
            else
                joinRace "${BOTNAME}" "${BOTKEY}" "${RACE_NAME}" "$CARS" "$BOT_MODE";
            fi
        done
    fi
}

main $@;
