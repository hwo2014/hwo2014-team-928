
function Server(host, port) {
  this.host = host || 'testserver.helloworldopen.com';
  this.port = port || '8091'
}

Server.prototype.toString = function () {
  return (JSON.stringify(this, null, 2));
};

module.exports = Server;
