/*
 * bootstrap the app by invoking its main method
 * and pasing it the pre-procesed arguments.
 */
require('./app').main(
  require('minimist')(
    process.argv.slice(2)
  )
);
