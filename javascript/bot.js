'use strict';

var logger = require('./logger');
var Server = require('./server');
var Client = require('./client');

var log    = logger;


var BotMath = {
  sign : function sign(n) {
    return (n ?
            ((n < 0) ? -1 : 1) :
            0)
  }
};

var flag = function flag(value, defaultValue) {
  return (
    (typeof value != 'undefined') ?
      ((('' + value).toLowerCase() === 'true') ||
       (('' + value).toLowerCase() === 'false') ? !!value : value) :
    (typeof defaultValue != 'undefined' ? defaultValue : false)
  );
};

// TODO: could improve to keep telemetrics on all cars, so we can
//       check if some are getting closer / further, if they have
//       turbos, etc...

function Bot(config, flags) {
  this.server      = config && config.server || new Server();
  this.client      = new Client(this.server);
  this.name        = config && config.name || 'bot';
  this.key         = config && config.key;
  this.race        = null;
  this.color       = 'red';
  this.config      = {
    laneSwitching   : flag(flags.laneSwitching, true),
    overtaking      : flag(flags.overtaking, true),
    turbo           : flag(flags.turbo, true),
    bumping         : flag(flags.bumping, true),
    breaking        : flag(flags.breaking, true),
    sneakiness      : flag(flags.sneakiness, true)
  };
  this.telemetrics = {
    tick     : 0,
    state    : true,        // true = OK, false = KO (DNF|crashed)
    turbo    : {
      active   : false,
      data     : null
    },
    status : {
      throttle : 1.0,
      angle    : 0.0,
      speed    : 0.0
    },
    position : {
      index   : 0,
      inPiece : 0.0,
      inLap   : 0.0,
      inRace  : 0.0
    },
    crashes  : {},          // recorded crash info, to adapt next passes
    old      : null         // copy of previous gameTick's telemetrics
  };
  log.info('Server : ', log.json(this.server));
  log.info('Bot    : ', log.json(this.id()));
}


Bot.prototype.id         = function id() {
  return ({
    name : this.name,
    key  : this.key
  });
};

Bot.prototype.raceData   = function raceData(trackName, password, carCount) {
  return ({
    botId     : this.id(),
    trackName : (trackName || 'keimola'),
    password  : (password || ''),
    carCount  : (carCount || 3)
  });
};

Bot.prototype.createRace = function createRace(trackName, password, carCount) {
  this.client.message('createRace', this.raceData(trackName, password, carCount));
};

Bot.prototype.join       = function join(trackName, password, carCount) {
  if (trackName) {
    this.client.message('joinRace', this.raceData(trackName, password, carCount));
  } else {
    /*
     * joins a dummy race or whichever one was created by the server
     */
    this.client.message('join', {
      name : this.name,
      key  : this.key
    });
  }
};

/**
 * Singles-out a car record for a given carName (or, by default, our car).
 */
Bot.prototype.car      = function car(data, carName) {
  return (data.filter(function myCarFilter(car) {
    return (car.id.name === (carName || this.name));
  }, this)[0]);
};

Bot.prototype.throttle   = function throttle(throttle) {
  if (throttle) {
    this.telemetrics.status.throttle = throttle;
  }
  this.client.message('throttle', this.telemetrics.status.throttle, this.telemetrics.tick);
};

// TODO: stack turbos we should stack turbos, as we can receive a new
// one before the turboEnd.  we can't fire it before the other
// expires, and we can't keep more than 1 turbo available at once, but
// to retain the turbo's data (for computation) we should stack them.
Bot.prototype.turbo      = function turbo(message) {
  this.telemetrics.turbo.active = true;
  this.client.message('turbo', message || 'C-C-C-Combo Breaker!!');
};

Bot.prototype.distance   = function distance(index) {
  return (
    ((typeof index !== 'undefined') ?
     this.race.track.pieces.slice(0, index) : // slice -> distance up to piece
     this.race.track.pieces                   // all pieces -> total distance
    )
      .map(function (e) {
        return (e.hasOwnProperty('length') ?
                e.length :
                // inexact: calculation for middle of track
                // which doesn't account for lanes
                ((e.angle * Math.PI * e.radius) / 180));
      })
      .reduce(function (l, r) {
        return (l + r);
      }, 0)
  );
};

Bot.prototype.resetTelemetrics = function resetTelemetrics() {
  this.telemetrics = {
    tick     : 0,
    state    : true,
    turbo    : {
      active   : false,
      data     : null
    },
    status : {
      throttle : 1.0,
      angle    : 0.0,
      speed    : 0.0
    },
    position : {
      index   : 0,
      inPiece : 0.0,
      inLap   : 0.0,
      inRace  : 0.0
    },
    crashes  : (this.telemetrics.crashes || {}),
    old      : null
  };
  return (this.telemetrics);
};

Bot.prototype.cloneTelemetrics = function cloneTelemetrics() {
  return ({
    tick     : this.telemetrics.tick,
    state    : this.telemetrics.state,
    turbo    : {
      active   : this.telemetrics.turbo.active,
      data     : this.telemetrics.turbo.data
    },
    status : {
      throttle : this.telemetrics.status.throttle,
      angle    : this.telemetrics.status.angle,
      speed    : this.telemetrics.status.speed
    },
    position : {
      index   : this.telemetrics.position.index,
      inPiece : this.telemetrics.position.inPiece,
      inLap   : this.telemetrics.position.inLap,
      inRace  : this.telemetrics.position.inRace
    },
    crashes  : this.telemetrics.crashes
  });
};

/**
 * Calculates position information from the car object
 * (returned by car() from the carPosition data).
 */
Bot.prototype.update = function update(carPositions, gameTick) {
  var car      = this.car(carPositions);

  this.telemetrics.old = this.cloneTelemetrics();
  if (typeof gameTick !== 'undefined') {
    this.telemetrics.tick = gameTick;
  }
  this.telemetrics.position = this.position(car);
  this.telemetrics.status.speed = this.speed();
  this.telemetrics.status.angle = car.angle;
  return (this.telemetrics);
};

/**
 * Calculates position information from the car object
 * (returned by car() from the carPosition data).
 */
Bot.prototype.position          = function position(car) {
  var position = {};

  if (car) {                    // gather position info for given car
    position.index   = car.piecePosition.pieceIndex;
    position.lap     = car.piecePosition.lap;
    position.inPiece = car.piecePosition.inPieceDistance;
    position.inLap   = this.distance(position.index) + position.inPiece;
    position.inRace  = (position.lap * this.distance()) + position.inLap;
  } else {                      // return known position info for my car
    position = this.telemetrics.position;
  }
  return (position);
};

Bot.prototype.printTelemetrics  = function printPositionInfo(car) {
  var position = (car ?
                  this.position(car) :        // get position for a given car
                  this.telemetrics.position);  // my position info
  var piece = this.race.track.pieces[position.index]

  log.verbose(
    '[%s] position (piece=%s/%s, lap=%s/%s)',
    this.telemetrics.tick,
    position.index, this.race.track.pieces.length,
    position.lap + 1, this.race.raceSession.laps);
  log.verbose(
    '     distance (piece=%s/%s, lap=%s/%s, race=%s/%s)',
    position.inPiece.toFixed(3),
    (piece.length || ((piece.angle * Math.PI * piece.radius) / 180)).toFixed(3),
    position.inLap.toFixed(3), ((position.lap + 1) * this.distance()).toFixed(3),
    position.inRace.toFixed(3), (this.race.raceSession.laps * this.distance()).toFixed(3));
  log.verbose(
    '     status(speed=%s, angle=%s, throttle=%s)',
    this.telemetrics.status.speed.toFixed(3),
    this.telemetrics.status.angle.toFixed(3),
    this.telemetrics.status.throttle.toFixed(5));
};

Bot.prototype.speed              = function speed(car) {
  var position = car ?
    this.position(car) :        // get position for a given car
    this.telemetrics.position;  // my position info

  return ((this.telemetrics.position.inRace - this.telemetrics.old.position.inRace) /
          (this.telemetrics.tick - this.telemetrics.old.tick));
};


// safeguards for extreme values to guard against after cumulative changes
Bot.prototype.applySafeGuards    = function applySafeGuards(throttle) {
  if (throttle < 0.15) {         // don't slow down this bad, it's unlikely
    throttle = 0.15;
    log.debug('safeguard:  < 0.15 -> 0.15');
  } else if (throttle > 1.0) {  // prevent against dodgy computations
    log.debug('safeguard:  > 1.0 -> 1.0');
    throttle = 1.0;
  }
  if (this.telemetrics.crashes['' + this.telemetrics.position.index] &&
      (this.telemetrics.position.inPiece < this.telemetrics.crashes[this.telemetrics.position.index].position)) {
    log.warn('safeguard: crash zone: %s to %s', throttle, throttle * 0.50);
    throttle *= 0.60;
  }
  if (this.telemetrics.turbo.active) {
    log.warn('safeguard: in turbo mode: tune down a bit');
    throttle *= (1.0 / (this.telemetrics.turbo.data.turboFactor)) * 1.75;
  }
  return (throttle);
};

Bot.prototype.piece          = function piece(index) {
  return ((index < this.race.track.pieces.length) ?
          this.race.track.pieces[index] :
          this.race.track.pieces[index - this.race.track.pieces.length]);
}

Bot.prototype.shouldUseTurbo = function shouldUseTurbo() {
  var index = this.telemetrics.position.index;

  return (
    this.config.turbo &&
    this.telemetrics.turbo && this.telemetrics.turbo.data &&
      !this.telemetrics.turbo.active && // don't want to use turbo if other turbo is active
      this.telemetrics.turbo.data.turboFactor > 1.0 && // guarding against negative or decreasing factors
      (this.telemetrics.turbo.data.turboDurationTicks <= 30) && // standard turbos only
      Array(this.telemetrics.position.index + 15).every(function (nextPieceOffset) {
        return (Math.abs(this.piece(index + nextPieceOffset).angle || 0) < 7);
      })//  ||
    // isCarInFront()
  );
};

// checking for a relatively straight line,
// where angles for the next 3 segments are
// rather weak
Bot.prototype.isSafeEnoughToFullThrottle = function isSafeEnoughToFullThrottle(p1, p2, p3) {
  var p1Angle = Math.abs(p1.angle || 0);
  var p2Angle = Math.abs(p2.angle || 0);
  var p3Angle = Math.abs(p3.angle || 0);

  log.verbose('piece n=%d (length=%s,angle=%s,radius=%s)',
              this.telemetrics.position.index,
              p1.length || 'N/A',
              p1.angle || 'N/A',
              p1.radius || 'N/A');
  log.verbose('piece n+1  (length=%s,angle=%s,radius=%s)',
              p2.length || 'N/A',
              p2.angle || 'N/A',
              p2.radius || 'N/A');
  log.verbose('piece n+2  (length=%s,angle=%s,radius=%s)',
              p3.length || 'N/A',
              p3.angle || 'N/A',
              p3.radius || 'N/A');
  return (p1Angle < 25 &&
          p2Angle < 20 &&
          p3Angle < 15);
};

// moronic implementation
// TODO: for now using indexes but this is entirely useless:
//       we should determine actions based on actual distances
// TODO: for now using stupid safeguards. actual speed and friction
//       measurements should be made
Bot.prototype.react      = function react(data) {
  if (data.msgType === 'carPositions') {
    var newThrottle = this.telemetrics.status.throttle;

    this.update(data.data, data.gameTick);
    this.printTelemetrics();
    if (data.hasOwnProperty('gameTick')) { // and have a gameTick to respond to
      var index = this.telemetrics.position.index;

      if (!this.telemetrics.state || index == 0) { // start, respawn, or wait
        newThrottle = 1.0;
      } else {
        var pieceCount = this.race.track.pieces.length;
        var p1 = this.piece(index);
        var p2 = this.piece(index + 1);
        var p3 = this.piece(index + 2);
        var p1Angle = Math.abs(p1.angle || 0);
        var p2Angle = Math.abs(p2.angle || 0);
        var p3Angle = Math.abs(p3.angle || 0);

        if (this.isSafeEnoughToFullThrottle(p1, p2, p3)) {
          newThrottle = 1.0;
          log.verbose(' ++++ super safe - 1.0 throttle');
          if (this.shouldUseTurbo()) {
            this.turbo();
            this.throttle(0.5);
            return ;
          }
        } else if ((p1Angle < 5) && p2Angle < 5) {
          // assuming (relatively) straight line in near future
          log.verbose(' ++++ rather safe - 0.8 throttle');
          newThrottle = 0.95;
        } else if ((p1Angle < 7) && p2Angle < 7) {
          // assuming (relatively) straight line in near future
          log.verbose(' ++++ rather safe - 0.8 throttle');
          newThrottle = 0.85;
        } else if ((p1Angle < 10) && p2Angle < 10) {
          // assuming (relatively) straight line in near future
          log.verbose(' ++++ rather safe - 0.8 throttle');
          newThrottle = 0.80;
        } else { // things get shaky: adapt a trend
          if (index != this.telemetrics.old.position.index) { // once per piece is enough
            var angleTrend = (p1Angle || 0.000001) / (p2Angle || 0.000001);

            log.verbose(' road angle trend: %s', angleTrend);
            newThrottle *= angleTrend;
          }
        }
        // dynamic throttling based on slip angle trend
        var newAngle  = Math.abs(this.telemetrics.status.angle);
        var oldAngle  = Math.abs(this.telemetrics.old.status.angle);
        var slipTrend = (oldAngle || 0.000001) / (newAngle || 0.000001);

        log.verbose(' slip angle trend: %s', slipTrend);
        newThrottle *= slipTrend;
        // safeguards to dramatically slow down if car slip angle is too wide
        if (slipTrend < 1.0) {
          newThrottle *=
            (newAngle > 90) ? 0.1 :
            (newAngle > 80) ? 0.15 :
            (newAngle > 70) ? 0.2 :
            (newAngle > 60) ? 0.25 :
            (newAngle > 50) ? 0.3 :
            (newAngle > 40) ? 0.35 :
            (newAngle > 30) ? 0.5 :
            (newAngle > 20) ? 0.9 :
            1.0;
        }
      }
      newThrottle = this.applySafeGuards(newThrottle);
      this.throttle(newThrottle);
    }
  } else if (data.msgType === 'gameStart') {
    log.info('game started');
    this.resetTelemetrics();
    this.throttle(1.0);         // don't bother: unleash horses, think later
  } else {
    this.gameEventHandler(data);
  }
};

Bot.prototype.isMe             = function isMe(data) {
  return ((data.name === this.name) && (data.color === this.color));
};

Bot.prototype.gameEventHandler = function gameEventHandler(data) {
  if (data.msgType === 'joinRace') {
    log.info('[%s] joined race %s',
             data.data.name,
             data.data.trackName);
  } else   if (data.msgType === 'createRace') {
    log.info('[%s] created race %s',
             data.data.name,
             data.data.trackName);
  } else if (data.msgType === 'spawn') {
    if (this.isMe(data.data)) {
      this.telemetrics.state = true;
    }
    log.info('[%s] car restored', data.data.name);
  } else if (data.msgType === 'crash') {
    if (this.isMe(data.data)) {
      this.telemetrics.state = false;
      if (!this.telemetrics.crashes['' + this.telemetrics.position.index]) {
        this.telemetrics.crashes['' + this.telemetrics.position.index] = {
          tick     : this.telemetrics.tick,
          position : this.telemetrics.position.inPiece,
          count    : 1
        };
      } else {
        var crash = this.telemetrics.crashes['' + this.telemetrics.position.index];

        crash.tick = this.telemetrics.tick;
        crash.position = this.telemetrics.position.inPiece;
        crash.count += 1;
        this.telemetrics.crashes['' + this.telemetrics.position.index] = crash;
      }
      log.warn('[%s] car crashed', data.data.name);
      //process.exit(0);
    } else {
      log.info('[%s] car crashed', data.data.name);
    }
  } else if (data.msgType === 'join') {
    log.info('[%s] bot joined', data.data.name);
  } else if (data.msgType === 'yourCar') {
    this.color = data.data.color;
    log.info('[%s] new car (%s)',
             data.data.name,
             data.data.color);
  } else if (data.msgType === 'turboStart') {
    if (this.isMe(data.data)) {
      this.telemetrics.turbo.active = true;
    }
    log.info('[%s] turbo start', data.data.name);
  } else if (data.msgType === 'turboEnd') {
    if (this.isMe(data.data)) {
      this.telemetrics.turbo.active = false;
      this.telemetrics.turbo.data = null;
    }
    log.info('[%s] turbo end', data.data.name);
  } else if (data.msgType === 'gameInit') {
    this.race = data.data.race;
    log.info('game initialized (id = %s)', data.gameId);
    log.info('  track  : %s', this.race.track.id);
    log.info('  laps   : %s', this.race.raceSession.laps);
    log.info('  lanes  : %s', this.race.track.lanes.length);
    log.info('  pieces : %s', this.race.track.pieces.length);
    log.info('  length : %s', this.distance().toFixed(3));
    this.race = data.data.race;
    if (log.verbose) {
      log.info('race info: ', log.json(this.race.track));
    }
    log.info('');
    this.saveTrack(this.race.track);
  } else if (data.msgType === 'gameEnd') {
    var crashes = this.telemetrics.crashes;
    log.info('game ended (race is over)');
    log.verbose(' crashes: %s',
                Object.keys(crashes).reduce(function (prev, key) {
                  return (prev + crashes[key].count);
                }, 0));;
  } else if (data.msgType === 'tournamentEnd') {
    log.info('tournament ended (all races are over)');
  } else if (data.msgType === 'error') {
    log.warn('server error: ', data);
  } else if (data.msgType === 'finish') {
    // indicates the last segment before finish line or actual finish line?
    log.info('[%s] finished', data.data.name);
  } else if (data.msgType === 'lapFinished') {
    // on or just after finish line?
    log.info('[%s] finished lap %s/%s', data.data.car.name, data.data.lapTime.lap + 1, this.race.raceSession.laps);
    log.info('  in %s ticks (total %s)', data.data.lapTime.ticks, data.data.raceTime.ticks);
    log.info('  in %s ms (total %s)', data.data.lapTime.millis, data.data.raceTime.millis);
    log.info('  as position %s/%s', data.data.ranking.overall, this.race.cars.length);
  } else if (data.msgType === 'turboAvailable') {
    if (this.telemetrics.state) {
      log.info('turbo available (%s ticks / %s ms, %sx)',
               data.data.turboDurationTicks,
               data.data.turboDurationMilliseconds,
               data.data.turboFactor);
      this.telemetrics.turbo.data = data.data;
    } else {
      log.info('turbo missed (%s ticks / %s ms, %sx)',
               data.data.turboDurationTicks,
               data.data.turboDurationMilliseconds,
               data.data.turboFactor);
    }
  } else if (data.msgType === 'dnf') {
    log.warn('[%s] DNF (%s)', data.data.car.name, data.data.reason);
  } else {
    log.warn('/!\\ yet unknown message: %s /!\\', data.msgType);
  }
  // unnecessary, except  maybe to prevent timeouts, but shouldn't occur
  // this.client.ping();
};

Bot.prototype.start      = function start(track, trackPassword, carCount, createRace) {
  var bot = this;

  log.info('[%s] connecting to server', this.name);
  if (this.client.connect()) {
    this.client.connection.on('connect', function onConnect() {
      log.info('[%s] connected to server', bot.name, bot.server);
      log.info('');
      if (typeof track !== 'undefined') {
        if (createRace) {
          bot.createRace(track, trackPassword || '', carCount || 1);
        } else {
          bot.join(track, trackPassword || '', carCount || 1);
        }
      } else {
        bot.join();             // default launch mode / auto
      }
    });
    this.client.receive(function onReceive(data) {
      bot.react(data);
    });
  }
};

Bot.prototype.saveTrack  = function saveTrack(track) {
  var fs   = require('fs');
  var file = 'maps/' + track.id + '.json';
  var json = log.json(track);

  if (!fs.exists(file) || (fs.readFileSync(file) != json)) {
    fs.writeFile(file, json, function(err) {
      if (err) {
        log.error('failed to save track %s to %s', track.name, file);
      } else {
        log.info('race track data saved to: %s', file);
      }
    });
  }
};


Bot.prototype.toString   = function toString() {
  return (JSON.stringify(this, null, 2));
};



module.exports = Bot;
