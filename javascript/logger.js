
var winston = require('winston');


var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      colorize : true,
      level    : 'debug'
    }),
    new (winston.transports.File)({
      filename : 'logs/bot.log',
      level    : 'info',
      json     : false
    }),
    new (winston.transports.File)({
      name     : 'file#debug',
      filename : 'logs/bot.debug.log',
      level    : 'debug',
      json     : false
    }),
    new (winston.transports.File)({
      name     : 'file#debug#json',
      filename : 'logs/bot.debug.json.log',
      level    : 'debug',
      json     : true
    }),
    new (winston.transports.File)({
      name     : 'file#error',
      filename : 'logs/bot.error.log',
      level    : 'warn',
      json     : false
    })
  ]
});

logger.json = function (object, indent) {
  return (logger.verbose ?
          JSON.stringify(
            object,
            null,
            ((typeof indent != 'undefined') ? indent : 2)
          ) :
          object
         );
};

logger.isVerbose = false;


module.exports = logger;
