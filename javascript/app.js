'use strict';

var logger = require('./logger');
var Bot    = require('./bot');
var Server = require('./server');

var log    = logger;

var flag = function flag(value, defaultValue) {
  return (
    (typeof value != 'undefined') ?
      ((('' + value).toLowerCase() === 'true') ||
       (('' + value).toLowerCase() === 'false') ? !!value : value) :
    (typeof defaultValue != 'undefined' ? defaultValue : false)
  );
};

function main(args) {
  log.isVerbose = flag(args.isVerbose, false);
  log.transports.console.level = flag(args.logLevel, 'info');
  log.info(Array(61).join('='));
  log.debug('args: ', args);
  new Bot({
    name   : args._[2],
    key    : args._[3],
    server : new Server(args._[0], args._[1])  // host, port
  }, args).start(
    args.track,
    args.trackPassword,
    args.carCount,
    args.createRace
  );
}


module.exports.main = main;
