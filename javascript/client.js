
var net  = require('net');

var json = require('JSONStream');

var log  = require('./logger');


function Client(serverInfo) {
  this.serverInfo = serverInfo;
  this.connection = null;
  this.input      = null;
}


Client.prototype.connect = function () {
  try {
    log.debug(' -- client - connect: ', this.serverInfo);
    this.connection = net.connect(this.serverInfo);
  } catch (e) {
    log.error('connection error: ', e);
    return (false);
  }
  this.input = this.connection.pipe(json.parse());
  return (this.connection);
};

Client.prototype.send    = function (json) {
  log.debug(' -- client - send: ', json);
  this.connection.write(JSON.stringify(json));
  return (this.connection.write('\n'));
};

Client.prototype.receive = function (handler) {
  return (this.input.on('data', function (data) {
    log.debug(' -- client - received: ', log.json(data));
    return (handler(data));
  }));
};

Client.prototype.error   = function error(handler) {
  return (this.connection.on('error', function () {
    log.error(' -- client - network error: ', arguments);
    return (handler.apply(null, arguments));
  }));
};

/**
 * Wrapper for the send() method, simplifying calls
 * to avoid repeating msgType.
 */
Client.prototype.message = function message(type, data, gameTick) {
  log.debug(' -- client - message:', type, (data || ''));
  return (this.send(typeof gameTick != 'undefined' ? {
    msgType  : type || '',
    data     : data || {},
    gameTick : gameTick
  } : {
    msgType  : type || '',
    data     : data || {}
  }));
};


/* Game Protocol Implementation Methods */

Client.prototype.ping      = function ping() {
  return (this.message('ping'));
};

Client.prototype.toString  = function toString() {
  return (JSON.stringify(this, null, 2));
};


module.exports = Client;
