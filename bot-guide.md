# Architecture and Random Notes

## Qualifying Round

The qualifying round is used to build an internal map of the track so
optimized trajectories, speed and overtaking points can be computed
for the actual race.

## Stored Tracks

If the track is already known, a stored map of track is loaded from a
local file so we can directly use optimal controls even during the
qualifying round.

To avoid mean stuff like tracks changing names or suddenly changing
their design though, we verfify that the track information sent back
by the server verifies against the stored map. If not, we default to
our safer strategies.

## Boosters

Booster strategies are rather simple:

 * if you enter a straight line, use the booster as soon as possible
   if its duration will allow to use it before the turn. If there's a
   car in front, make sure to switch lanes to use this as an occasion
   to overtake.

 * if you aren't in a straight line, and there's a car in front of
   you, use the booster to bump her off track.

 * if you are in a straight line, but the line's too short for the
   booster's duration, we can either risk overtaking at high speed (if
   the other line is free) or we can risk bumping into the front car
   so that booster at least is useful to grab some headway on cars
   behind us.

_NOTES:_

 * are we aware of the other cars' booster statuses?

## Strategies

The bot uses several strategies, possiblty simultaneously.


# Command Clarification

 * Bots can send back only one message per tick. If more are received,
   all are ignored, except for the first one. One reply and one action
   per tick!

 * Bots should only reply to:
    * {{gameStart}},
    * and {{carPositions}}
   with **either** one of:
    * {{throttle}},
    * {{turbo}},
    * {{switchLane}}.

 * {{ping}} seem to be effectively useless (and possibly harmful).

# Tech Spec Clarifications and Interesting Questions

 * http://www.reddit.com/r/HWO/comments/23u9du/may_i_assume_that_the_tournament_races_behavior/

    > We're working on eliminating latency between the server and the
    > bot on the CI and the race so that we could give a more exact
    > number. Right now I'd say that you should aim at answering within
    > 10 milliseconds. In the finals, the race will be a fixed 60
    > ticks/sec anyway so you shouldn't be doing too heavy computation
    > per tick. I hope this helps and sorry for not being able to give
    > an exact number.

# Logging

Logging by default is enabled for the console and several files:

 - {{javascript/logs/bot.log}} - normal execution log ({{info}} level)
 - {{javascript/logs/bot.debug.log}} - all logs ({{debug}} level)
 - {{javascript/logs/bot.error.log}} - error logs ({{warn}} level)

_TODO:_

 * does the CI or official runs allow to write files? If not, by
   default we should only output to console.